FROM python:3.7

ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG WORKING_DIR

ADD ./pip_requirements.txt /pip_requirements.txt

RUN : "必要そうなものをインストールしておいたが、本当に必要なのかは分からない" && \
    apt-get update -qq && \
    apt-get install -y build-essential libxslt-dev libxml2-dev cmake gnupg && \
    : "node.jsとnpmのインストール" && \
    curl -sL https://deb.nodesource.com/setup_11.x | bash - && \
    apt-get install -y nodejs && \
    npm install npm@latest -g && \
    : "aws-cliなどのデプロイするアプリケーションに関係のないpipライブラリのインストール" && \
    pip install -r /pip_requirements.txt && \
    : "Serverless Frameworkをインストール" && \
    npm install -g serverless && \
    : "ecs-cliのインストール" && \
    curl -o /usr/local/bin/ecs-cli https://amazon-ecs-cli.s3.amazonaws.com/ecs-cli-linux-amd64-latest && \
    chmod +x /usr/local/bin/ecs-cli && \
    : "Serverless Frameworkで使用するAWSアカウントの設定" && \
    sls config credentials --provider aws --key $AWS_ACCESS_KEY_ID --secret $AWS_SECRET_ACCESS_KEY

WORKDIR $WORKING_DIR