# Serverless Frameworkを使ってLambdaにPythonアプリケーションをデプロイするためのコンテナ

## 基本的なコンテナの使い方

### コンテナの起動

```bash
docker-compose build
```

### コンテナのシェルにログイン
```bash
docker-compose run deploy_python_app_to_lambda bash
```

## サービスを作成する

### Pythonインタプリンタを用いてサービスを作成する
```bash
sls create -t aws-python -p "サービス名"
```

## デプロイ
```bash
cd "サービス名" && sls deploy
```

## テスト

### 基本形
```bash
sls invoke -f hello
```

### データを送る時
```bash
sls invoke -f hello -d '{"key":"value"}'
```

## 環境変数

### パラメータストアに安全な文字列として保存した値を取得する

以下のコマンドでAWS KMS の暗号化キーを作成

```bash
aws kms create-key --description "Sample key"
```

作成した暗号化キーをエイリアスに登録
(KeyId は、` aws kms create-key `コマンドの結果表示された `KeyId` )

```bash
aws kms create-alias --alias-name alias/sample-key --target-key-id "KeyId"
```

もしエイリアスを上書きしたい場合は

```bash
aws kms update-alias  --alias-name alias/sample-key --target-key-id "KeyId"
```

作成した暗号化キーを用いてパラメータを作成

```bash
aws ssm put-parameter --name "MY_SECRET1" \
    --description "安全な文字列パラメータ" \
    --type "SecureString" \
    --value "Secure string parameter" \
    --key-id "alias/sample-key"\
    --overwrite #上書きしたい場合必要があれば
```

(暗号化しなくてもいい場合)
```bash
aws ssm put-parameter --name "MY_SECRET2" \
    --description "文字列パラメータ" \
    --type "String" \
    --value "String parameter"
```

作成した暗号化キーを用いてパラメータを作成

```yaml
environment:
  MY_SECRET1: ${ssm:MY_SECRET1~true}
  MY_SECRET2: ${ssm:MY_SECRET}
     
```

暗号化キーの削除
```bash
aws kms schedule-key-deletion --key-id KeyId --pending-window-in-days 7
```

## pip install
```bash
pip instal boto3 -t ./
```